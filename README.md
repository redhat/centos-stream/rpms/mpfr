# mpfr

The [MPFR library](https://www.mpfr.org/) is a C library for
multiple-precision floating-point computations with _correct rounding_.  MPFR
has continuously been supported by [INRIA](https://www.inria.fr/) and the
current main authors come from the [Caramba](https://caramba.loria.fr/) and
[AriC](http://www.ens-lyon.fr/LIP/AriC/) project teams at
[Loria](https://www.loria.fr/) (Nancy, France) and
[LIP](http://www.ens-lyon.fr/LIP/) (Lyon, France) respectively; see more on
the [credit page](https://www.mpfr.org/credit.html).  MPFR is based on the
[GMP](https://gmplib.org/) multiple-precision library.

The main goal of MPFR is to provide a library for multiple-precision
floating-point computation which is both efficient and has a well-defined
semantics.  It copies the good ideas from the ANSI/IEEE-754 standard for
double-precision floating-point arithmetic (53-bit significand).
